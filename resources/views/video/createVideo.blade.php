@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h2>Crear un nuevo video</h2>
            <form action="{{ route('storeVideo')  }}" method="post" enctype="multipart/form-data" class="col-lg-7">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label for="title">Titulo</label>
                    <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea id="description" name="description" class="form-control">{{ old('description') }}</textarea>
                </div>
                <div class="form-group">
                    <label for="image">Miniatura</label>
                    <input type="file" id="image" name="image" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="video">Archivos de video</label>
                    <input type="file" id="video" name="video" class="form-control"/>
                </div>
                <button type="submit" class="btn btn-success"> Crear video</button>
            </form>
        </div>

    </div>
@stop