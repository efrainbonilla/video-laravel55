@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-10 pull-left panel panel-default">
                    <div class="row" style="padding-bottom: 10px; padding-top: 10px;">


                        @if(isset($search))
                        <div class="col-md-8">
                                @if(count($videos)>0)
                                    <p>Resultados de la busqueda: '<strong>{{ $search }}</strong>'</p>
                                @else
                                    <p>No se encontro resultados para la busqueda: '<strong>{{ $search }}</strong>'</p>
                                @endif
                        </div>
                        <div class="col-md-4">
                            @if(count($videos)>0)
                            <form class="col-lg-10 pull-right" action="{{ route('videoSearch', ['search' => $search]) }}" method="get">
                                <label for="filter">Ordenar</label>
                                <select name="filter" id="filter" class="form-control">
                                    <option value="new">Mas nuevo primero</option>
                                    <option value="old">Mas antiguo primero</option>
                                    <option value="alfa">De la A a la Z</option>
                                </select>
                                <input type="submit" value="Ordenar" class="btn btn-sm btn-primary">
                            </form>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>

                @include('video.videoList')
        </div>
    </div>
@endsection
