@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h2>Editar {{ $video->title }}</h2>
            <form action="{{ route('updateVideo', ['id' => $video->id])  }}" method="post" enctype="multipart/form-data" class="col-lg-7">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label for="title">Titulo</label>
                    <input type="text" id="title" name="title" value="{{ $video->title }}" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea id="description" name="description" class="form-control">{{ $video->description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="image">Miniatura</label>
                    @if(Storage::disk('images')->has($video->image))
                        <div class="video-image-thumb ">
                            <div class="video-image-mask">
                                <img src="{{ route('imageVideo', ['filename' => $video->image]) }}" class="">
                            </div>
                        </div>
                    @endif
                    <input type="file" id="image" name="image" class="form-control"/>
                </div>
                <div class="form-group">

                    <label for="video">Archivos de video</label>
                    <video controls id="video-player">
                        <source src="{{ route('fileVideo', ['filename' => $video->video_path]) }}"><source>
                        Tu navegador no es compatible con html5
                    </video>
                    <input type="file" id="video" name="video" class="form-control"/>
                </div>
                <button type="submit" class="btn btn-success"> Editar video</button>
            </form>
        </div>
    </div>
@stop