<div id="videos-list">
    @foreach($videos as $video)
        <div class="video-item col-md-10 pull-left panel panel-default">
            <div class="panel-body">
                {{--imagen del video--}}
                @if(Storage::disk('images')->has($video->image))
                    <div class="video-image-thumb col-md-4 pull-left">

                        <div class="video-image-mask">
                            <img src="{{ route('imageVideo',  $video->image) }}" class="video-image"/>
                        </div>

                    </div>
                @endif
                <div class="data">
                    <h4><a class="video-title" href="{{ route('detailVideo', ['video_id' => $video->id]) }}">{{ $video->title  }}</a></h4>
                    <p><a href="{{ route('channel', ['user_id'=> $video->user_id]) }}">{{ $video->user->name . ' ' . $video->user->surname }}</a></p>
                </div>
                {{--botones de accion--}}
                <a href="{{ route('detailVideo', ['video_id' => $video->id]) }}" class="btn btn-success">Ver</a>
                @if(Auth::check() && Auth::user()->id == $video->user->id)
                    <a href="{{ route('videoEdit', ['id' => $video->id]) }}" class="btn btn-warning">Editar</a>


                    <a href="#victorModal{{ $video->id }}" role="button" class="btn btn-danger" data-toggle="modal">Eliminar</a>

                    <!-- Modal / Ventana / Overlay en HTML -->
                    <div id="victorModal{{ $video->id }}" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">¿Estás seguro?</h4>
                                </div>
                                <div class="modal-body">
                                    <p>¿Seguro que quieres borrar este video?</p>
                                    <p class="text-warning"><small>{{ $video->body }}</small></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <a href="{{ url('/delete-video/' . $video->id)  }}" class="btn btn-danger">Eliminar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

        </div>
    @endforeach
    {{ $videos->links() }}
</div>