<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

use App\Video;
use App\Comment;

class VideoController extends Controller
{
    //
    public function createVideo() {
        return view('video.createVideo');
    }

    public function storeVideo(Request $request) {
        //validar formulario

        $validateData = $request->validate([
            'title' => 'required|min:5',
            'description' => 'required',
            'video' => 'mimes:mp4',
            'image' => 'mimes:jpg,jpeg,png'
        ]);


        $video = new Video();
        $user = \Auth::user();
        $video->user_id = $user->id;
        $video->title = $request->input('title');
        $video->description = $request->input('description');

        //subida de la miniatura
        $image = $request->file('image');
        if ($image) {
            $image_path = time() . $image->getClientOriginalName();
            \Storage::disk('images')-> put($image_path, \File::get($image));

            $video->image = $image_path;
        }


        //subida de el video
        $video_file = $request->file('video');
        if ($video_file) {
            $video_path = time(). $video_file->getClientOriginalName();
            \Storage::disk('videos')->put($video_path, \File::get($video_file));

            $video->video_path = $video_path;
        }

        $video->save();

        return redirect()->route('home')->with([
            'message' => 'El video se ha subido correctamente.'
        ]);
    }

    public function getImage($filename){
        $file = Storage::disk('images')->get($filename);
        return new Response($file, 200);
    }

    public function getVideoDetail($video_id) {
        $video = Video::find($video_id);

        return view('video.detail', ['video' => $video]);

    }

    public function getVideo($filename){
        $file = Storage::disk('videos')->get($filename);
        return new Response($file, 200);

    }

    public function delete($video_id) {
        $user = \Auth::user();

        $video = Video::find($video_id);
        $comments = Comment::where('video_id', $video_id)->get();

        if ($user && ($video->user_id == $user->id)) {

            //eliminar comentarios
            if ($comments && count($comments) >= 1)
                foreach ($comments as $comment) {

                    $comment->delete();
                }

            //eliminar ficheros
            Storage::disk('images')->delete($video->image);
            Storage::disk('videos')->delete($video->video_path);

            //eliminar registros
            $video->delete();
            $message = ['message' => 'Vídeo eliminado correctamente.'];
        } else {
            $message = ['message' => 'Error, no se pudo eliminar el registro'];
        }

        return redirect()->route('home')->with($message);

    }

    public function edit($id) {
        $user = \Auth::user();
        $video = Video::find($id);
        if ($user && $user->id == $video->user_id) {


            return view('video.edit', ['video' => $video]);

        } else {
            return redirect()->route('home');
        }

    }

    public function update($video_id, Request $request) {

        $validateData = $request->validate([
            'title'  => 'required|min:5',
            'description' => 'required',
            'video' => 'mimes:mp4'
        ]);



        $user =  \Auth::user();
        $video = Video::findOrFail($video_id);


        $video->user_id = $user->id;
        $video->title = $request->input('title');
        $video->description = $request->input('description');


        //subida de la miniatura
        $image = $request->file('image');
        if ($image) {
            $image_path = time() . $image->getClientOriginalName();
            \Storage::disk('images')-> put($image_path, \File::get($image));

            Storage::disk('images')->delete($video->image);
            $video->image = $image_path;
        }


        //subida de el video
        $video_file = $request->file('video');
        if ($video_file) {
            $video_path = time(). $video_file->getClientOriginalName();
            \Storage::disk('videos')->put($video_path, \File::get($video_file));

            Storage::disk('videos')->delete($video->video_path);

            $video->video_path = $video_path;
        }

        $video->update();

        return redirect()->route('home')->with(['message' => 'EL video se ha actualizado correctamente!!']);

    }

    public function search($search = null, $filter = null) {

        if (is_null($search)) {
            $search = \Request::get('search');
            if (is_null($search)){
                return redirect()->route('home');
            }

            return redirect()->route('videoSearch', ['search' => $search]);
        }

        if (is_null($filter) && \Request::get('filter') && !is_null($search)) {
            $filter = \Request::get('filter');

            return redirect()->route('videoSearch', ['search' => $search, 'filter' => $filter]);
        }

        $column = 'id';
        $value = 'DESC';
        if (!is_null($filter)) {
            if ($filter == 'new') {
                $column = 'id';
                $value = 'DESC';
            } elseif ($filter == 'old') {
                $column = 'id';
                $value = 'ASC';
            } elseif ($filter == 'alfa') {
                $column = 'title';
                $value = 'ASC';
            }
        }

        $videos = Video::where('title', 'like', '%'.$search.'%')->orderBy($column, $value)->paginate(5);


        return view('video.search', [
            'videos' => $videos,
            'search' => $search
        ]);
    }
}

