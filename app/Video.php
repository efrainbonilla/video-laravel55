<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';

    //Relation OneToMany
    public function comments() {
        return $this->hasMany('App\Comment')->orderBy('id', 'DESC');
    }

    //Relation ManyToOne
    public function user() {
        return$this->belongsTo('App\User', 'user_id');
    }


}
