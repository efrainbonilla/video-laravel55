<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Video;

/*Route::get('/', function () {

    return view('welcome');
});*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/video/create', [
    'as' => 'createVideo',
    'middleware' => 'auth',
    'uses' => 'VideoController@createVideo'
]);


Route::post('/video', [
    'as' => 'storeVideo',
    'middleware' => 'auth',
    'uses' => 'VideoController@storeVideo'
]);

Route::get('/miniatura/{filename}', [
    'as'=> 'imageVideo',
    'uses' => 'VideoController@getImage'
]);

Route::post('/video/{video_id}', [
    'as' => 'updateVideo',
    'middleware' => 'auth',
    'uses' => 'VideoController@update'
]);

Route::get('/video/{video_id}', [
    'as' => 'detailVideo',
    'uses' => 'VideoController@getVideoDetail'
]);

Route::get('/video-file/{filename}', [
   'as' => 'fileVideo',
   'uses' => 'VideoController@getVideo'
]);




Route::post('/comment', [
    'as' => 'comment',
    'mddleware' => 'auth',
    'uses' => 'CommentController@store'
]);

Route::get('/delete-comment/{id}', [
   'as' => 'commentDelete',
   'middleware' => 'auth',
   'uses' => 'CommentController@delete'
]);

Route::get('/delete-video/{id}', [
   'as' => 'videoDelete',
   'middleware' => 'auth',
   'uses' => 'VideoController@delete'
]);

Route::get('/editar-video/{id}', [
    'as' => 'videoEdit',
    'middleware' => 'auth',
    'uses' => 'VideoController@edit'
]);


Route::get('/buscar/{search?}/{filter?}', [
   'as' => 'videoSearch',
   'uses' => 'VideoController@search'
]);

Route::get('/channel/{user_id}', [
    'as' => 'channel',
    'uses' => 'UserController@channel'
]);


//cache
Route::get('/cc', function (){
   $code = Artisan::call('cache:clear');
   if ($code == 0){
       return 'Cache borrado exitosamente.';
   } else {
       return 'Error al borrar cache.';
   }

});